variable "ami" {
  type        = string
  description = "Linux AMI ID in us-east-1 Region"
  default     = "ami-0230bd60aa48260c6"
}

variable "instance_type" {
  type        = string
  description = "Instance type"
  default     = "t2.micro"
}

variable "name_tag" {
  type        = string
  description = "Name of the EC2 instance"
  default     = "My EC2 Instance"
}